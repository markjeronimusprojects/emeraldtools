/*
 * This file is part of EmeraldTools.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.emeraldTools.recipe;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import org.digitalmodular.emeraldTools.util.Util;

/**
 * @author Mark Jeronimus
 */
// Created 2016-11-12
public class EmeraldHoeRecipe extends HoeRecipe {
	public EmeraldHoeRecipe() {
		super(Material.EMERALD, makeResult());
	}

	private static ItemStack makeResult() {
		ItemStack tool = new ItemStack(Material.DIAMOND_HOE, 1);
		Util.setItemName(tool, ChatColor.COLOR_CHAR + "r" + ChatColor.GREEN + "Emerald Hoe");
		Util.setItemLore(tool, "Je persoonlijke\nlandbouwmachine!");
		return tool;
	}
}
