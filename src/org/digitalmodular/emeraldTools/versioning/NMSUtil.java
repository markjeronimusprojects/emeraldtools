/*
 * This file is part of EmeraldTools.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.emeraldTools.versioning;

import org.bukkit.Bukkit;

/**
 * @author Mark Jeronimus
 */
// Created 2016-11-10
public enum NMSUtil {
	;

	public static NMSTools getInstance() {
		String serverVersion = getServerPackageVersion();

		switch (serverVersion) {
			case "v1_10_R1":
				return new NMSTools_v1_10_R1();
			case "v1_9_R2":
				return new NMSTools_v1_9_R2();
			case "v1_9_R1":
				return new NMSTools_v1_9_R1();
			case "v1_8_R3":
				return new NMSTools_v1_8_R3();
			case "v1_8_R2":
				return new NMSTools_v1_8_R2();
			case "v1_8_R1":
				return new NMSTools_v1_8_R1();
			case "v1_7_R4":
				return new NMSTools_v1_7_R4();
			case "v1_7_R3":
				return new NMSTools_v1_7_R3();
			case "v1_7_R2":
				return new NMSTools_v1_7_R2();
			case "v1_7_R1":
				return new NMSTools_v1_7_R1();
			default:
				throw new IllegalVersionException("Plugin is not suitable for server version: " + serverVersion);
		}
	}

	public static String getServerPackageVersion() {
		String serverPackageVersion = tryGetServerPackageVersion("org.bukkit.craftbukkit.");

		if (serverPackageVersion == null) {
			String version = Bukkit.getServer().getVersion();
			throw new IllegalVersionException("Plugin is not suitable for this server version: " + version);
		}

		return serverPackageVersion;
	}

	private static String tryGetServerPackageVersion(String prefix) {
		String version = Bukkit.getServer().getClass().getPackage().getName();

		if (!version.startsWith(prefix))
			return null;

		return version.substring(prefix.length());
	}
}
