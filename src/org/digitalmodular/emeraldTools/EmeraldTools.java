/*
 * This file is part of EmeraldTools.
 *
 * Copyleft 2016 Mark Jeronimus. All Rights Reversed.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.digitalmodular.emeraldTools;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import org.digitalmodular.emeraldTools.recipe.EmeraldAxeRecipe;
import org.digitalmodular.emeraldTools.recipe.EmeraldHoeRecipe;
import org.digitalmodular.emeraldTools.recipe.EmeraldPickaxeRecipe;
import org.digitalmodular.emeraldTools.recipe.EmeraldShovelRecipe;
import org.digitalmodular.emeraldTools.versioning.NMSTools;
import org.digitalmodular.emeraldTools.versioning.NMSUtil;

/**
 * @author Mark Jeronimus
 */
// Created 2016-11-12
public class EmeraldTools extends JavaPlugin {
	@Override
	public void onEnable() {
		NMSTools nmsTools = NMSUtil.getInstance();

		registerRecipes();

		getLogger().info("Enabled");
	}

	@Override
	public void onDisable() {
		getLogger().info("Disabled");
	}

	private static void registerRecipes() {
		Bukkit.addRecipe(new EmeraldAxeRecipe());
		Bukkit.addRecipe(new EmeraldHoeRecipe());
		Bukkit.addRecipe(new EmeraldPickaxeRecipe());
		Bukkit.addRecipe(new EmeraldShovelRecipe());
	}
}
